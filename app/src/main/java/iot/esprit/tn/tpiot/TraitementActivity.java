package iot.esprit.tn.tpiot;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.sql.Time;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import iot.esprit.tn.tpiot.API.APIUrl;
import iot.esprit.tn.tpiot.API.IIOTService;
import iot.esprit.tn.tpiot.API.RetrofitBuild;
import iot.esprit.tn.tpiot.Model.Valeurs;
import iot.esprit.tn.tpiot.ResultModel.ResultValeur;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;

public class TraitementActivity extends AppCompatActivity {

    @BindView(R.id.temp)
    TextView temp;
    @BindView(R.id.Lum)
    TextView lum;
    @BindView(R.id.Refresh)
    Button refresh;
    @BindView(R.id.green)
    ImageView green;
    @BindView(R.id.red)
    ImageView red;
    @BindView(R.id.seuil_temp)
    EditText Seui_temp;
    @BindView(R.id.seuil_lum)
    EditText Seuil_lum;
    @BindView(R.id.Btn_seuil_temp)
    Button btn_temp;
    @BindView(R.id.Btn_seuil_lum)
    Button btn_lum;
    @BindView(R.id.Btn_msg)
    Button btn_msg;
    @BindView(R.id.msg)
    EditText msg;
    @BindView(R.id.led)
    ToggleButton led;
    @BindView(R.id.buz)
    ToggleButton buz;
    @BindView(R.id.Sound)
    TextView sound;
    String tempSt,LumSt,SoundSt;
    ProgressDialog progressDialog;
    private Handler etat;
    Timer timer ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traitement);
        ButterKnife.bind(this);
        msg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!charSequence.toString().isEmpty()&&charSequence.toString().length()<17){

                    btn_msg.setEnabled(true);
                }else {
                    btn_msg.setEnabled(false);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
        //android.support.v7.app.ActionBar bar = getSupportActionBar();
        //bar.setTitle("test");
       // bar.setBackgroundDrawable(new ColorDrawable(0xffff0000));
        //Utils.darkenStatusBar(this, R.color.Opaquered);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading..");
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartEtat();

            }
        });
        Timer timer =new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                ChckEtat();
                ChckEtatLed();
                ChckEtatBuzzer();
                SendAlertTemp();
                SendAlertLum();

            }
        },0,1000);
       buz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
               if (b){
                   StartBuzzer();
               }else {
                   StopBuzzer();
               }
           }
       });
       
       led.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
               if (b){
                   StartLed();
               }else {
                   StopLed();
               }
           }
       });
        btn_temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddSeuilTemp();
            }
        });

        btn_lum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddSeuilLum();
            }
        });
        btn_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendMessage();
            }
        });

    }

    private void SendAlertLum() {
        int seui_lum = getSeuilTemp();
    }

    private int getSeuilTemp() {
        return 0;
    }

    private void SendAlertTemp() {

    }

    private void SendMessage() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.setMesg(msg.getText().toString());

        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                if (response.body().getSuccess()){
                    Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {

            }
        });
    }

    private void StopLed() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.pausetLed();
        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                if (response.body().getMessage().equals("etat changed")){
                    Toast.makeText(getApplicationContext(),"etat changed",Toast.LENGTH_SHORT).show();
                    progressDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            LoadData();
                        }
                    }, 3000);

                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"fail:  "+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void StartLed() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.startLed();
        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                if (response.body().getMessage().equals("etat changed")){
                    Toast.makeText(getApplicationContext(),"etat changed",Toast.LENGTH_SHORT).show();
                    progressDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            LoadData();
                        }
                    }, 3000);

                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"fail:  "+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
        
    }

    private void StopBuzzer() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.pauseBuzrzer();
        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                if (response.body().getMessage().equals("etat changed")){
                    Toast.makeText(getApplicationContext(),"etat changed",Toast.LENGTH_SHORT).show();
                    progressDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            LoadData();
                        }
                    }, 3000);

                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"fail:  "+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void StartBuzzer() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.startBuzrzer();
        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                if (response.body().getMessage().equals("etat changed")){
                    Toast.makeText(getApplicationContext(),"etat changed",Toast.LENGTH_SHORT).show();
                    progressDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            LoadData();
                        }
                    }, 3000);

                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"fail:  "+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void AddSeuilLum() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.setSeuilLum(Integer.valueOf(Seuil_lum.getText().toString()));

        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                if (response.body().getSuccess()){
                    Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {

            }
        });
    }

    private void AddSeuilTemp() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.setSeuilTemp(Integer.valueOf(Seui_temp.getText().toString()));
        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                if (response.body().getSuccess()){
                    Toast.makeText(getApplicationContext(),"Success",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {

            }
        });
    }

    private void StartEtat() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.startEtat();
        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                if (response.body().getMessage().equals("etat changed")){
                    Toast.makeText(getApplicationContext(),"etat changed",Toast.LENGTH_SHORT).show();
                    progressDialog.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            LoadData();
                        }
                    }, 3000);

                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"fail:  "+t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void LoadData() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.getValues();
        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                if (response.body().getSuccess()){
                    Valeurs v = response.body().getValeurs();
                    tempSt = String.valueOf(v.getValtemp());
                    LumSt = String.valueOf(v.getVallum());
                    SoundSt = String.valueOf(v.getValsound());
                    lum.setText(LumSt);
                    temp.setText(tempSt);
                    sound.setText(SoundSt);

                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {

            }
        });
    }

    private void ChckEtat() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<ResultValeur> call = service.getEtat();
        call.enqueue(new Callback<ResultValeur>() {
            @Override
            public void onResponse(Call<ResultValeur> call, Response<ResultValeur> response) {
                if (response.body().getEtat()==1){
                    red.setVisibility(View.GONE);
                    green.setVisibility(View.VISIBLE);
                }else {
                    red.setVisibility(View.VISIBLE);
                    green.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResultValeur> call, Throwable t) {

            }
        });
    }

    private void ChckEtatLed() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<String> call = service.getEtatLed();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body().equals("1")){
                    led.setChecked(true);
                }else {
                    led.setChecked(false);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void ChckEtatBuzzer() {
        IIOTService service = RetrofitBuild.getClient(APIUrl.BASE_URL).create(IIOTService.class);
        Call<String> call = service.getEtatBuzzer();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.body().equals("1")){
                    buz.setChecked(true);
                }else {
                    buz.setChecked(false);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }



}
