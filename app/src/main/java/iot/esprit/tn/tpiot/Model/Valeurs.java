package iot.esprit.tn.tpiot.Model;

/**
 * Created by root on 15/02/2018.
 */

public class Valeurs {
    private  float vallum;
    private  float valtemp;
    private  float valsound;

    public Valeurs(float vallum, float valtemp) {
        this.vallum = vallum;
        this.valtemp = valtemp;
    }

    public Valeurs(float vallum, float valtemp, float valsound) {
        this.vallum = vallum;
        this.valtemp = valtemp;
        this.valsound = valsound;
    }

    public float getVallum() {
        return vallum;
    }

    public void setVallum(float vallum) {
        this.vallum = vallum;
    }

    public float getValtemp() {
        return valtemp;
    }

    public void setValtemp(float valtemp) {
        this.valtemp = valtemp;
    }

    public float getValsound() {
        return valsound;
    }

    public void setValsound(float valsound) {
        this.valsound = valsound;
    }
}
