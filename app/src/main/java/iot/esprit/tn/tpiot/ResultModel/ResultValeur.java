package iot.esprit.tn.tpiot.ResultModel;

import android.widget.ListView;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import iot.esprit.tn.tpiot.Model.Valeurs;

/**
 * Created by root on 15/02/2018.
 */

public class ResultValeur {
    @SerializedName("success")
    private Boolean success;

    @SerializedName("msg")
    private String message;

    @SerializedName("valeurs")
    private Valeurs valeurs;

    @SerializedName("etat")
    private  int etat;

    public ResultValeur(int etat) {
        this.etat = etat;
    }

    public ResultValeur(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public ResultValeur(String message) {
        this.message = message;
    }

    public ResultValeur(Boolean success, String message, Valeurs valeurs) {
        this.success = success;
        this.message = message;
        this.valeurs = valeurs;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Valeurs getValeurs() {
        return valeurs;
    }

    public void setValeurs(Valeurs valeurs) {
        this.valeurs = valeurs;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
}
