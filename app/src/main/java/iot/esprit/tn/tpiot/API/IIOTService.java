package iot.esprit.tn.tpiot.API;

import iot.esprit.tn.tpiot.ResultModel.ResultValeur;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by root on 15/02/2018.
 */

public interface IIOTService {
    @GET("/start_etat")
    Call<ResultValeur> startEtat();

    @GET("/get_last_valeurs")
    Call<ResultValeur> getValues();

    @GET("/get_card_state")
    Call<ResultValeur> getEtat();

    @GET("/insert_seuil_temp")
    Call<ResultValeur> setSeuilTemp(
            @Query("seuil") int seuil
    );

    @GET("/insert_seuil_lum")
    Call<ResultValeur> setSeuilLum(
            @Query("seuil") int seuil
    );

    @GET("/insert_message")
    Call<ResultValeur> setMesg(
            @Query("msg") String msg
    );

    @GET("/start_led")
    Call<ResultValeur> startLed();
    @GET("/start_buzzer")
    Call<ResultValeur> startBuzrzer();
    @GET("/pause_led")
    Call<ResultValeur> pausetLed();
    @GET("/pause_buzzer")
    Call<ResultValeur> pauseBuzrzer();
    @GET("/get_led_state")
    Call<String> getEtatLed();
    @GET("/get_buzzer_state")
    Call<String> getEtatBuzzer();
    @GET("/get_last_seuillum")
    Call<String> getSeuiLum();
    @GET("/get_last_seuil")
    Call<String> getSeuitemp();
}
